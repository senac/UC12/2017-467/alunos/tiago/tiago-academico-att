package br.com.senac.academico.bean;

import br.com.senac.academico.dao.AlunoDAO;
import br.com.senac.academico.model.Aluno;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "pesquisaAlunoBean")
@ViewScoped
public class PesquisaAlunoBean implements Serializable {

   private Aluno alunoSelecionado;
    private List<Aluno> lista;
    private AlunoDAO dao;

    private String codigo;
    private String nome;

    public PesquisaAlunoBean() {
    }

    @PostConstruct
    public void init() {
        try {
            dao = new AlunoDAO();
            alunoSelecionado = new Aluno();
            lista = dao.findAll();

        } catch (Exception ex) {
            ex.printStackTrace();
            
        }

    }

    public void pesquisa() {
        try {
            this.lista = this.dao.findByFiltro(codigo, nome);

        }catch(Exception ex){
            ex.printStackTrace();
            
        }

    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Aluno getAlunoSelecionado() {
        return alunoSelecionado;
    }

    public void setAlunoSelecionado(Aluno alunoSelecionado) {
        this.alunoSelecionado = alunoSelecionado;
    }

    public List<Aluno> getLista() {
        return lista;
    }

    public void setLista(List<Aluno> lista) {
        this.lista = lista;
    }

}
