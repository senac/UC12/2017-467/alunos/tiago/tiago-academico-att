package br.com.senac.academico.dao;

import br.com.senac.academico.model.Curso;
import br.com.senac.academico.model.Professor;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

// Criando conexão com o banco de dados!
public class JPAUtil {

    private static EntityManagerFactory enf = Persistence.createEntityManagerFactory("AcademicoPU");

    public static EntityManager getEntityManager() {

        try {

            return enf.createEntityManager();

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Erro ao acessar o banco de dados!");
        }

    }

    public static void main(String[] args) {
        
        ProfessorDAO prof = new ProfessorDAO();
        Professor p = prof.find(1);
        
        CursoDAO dao = new CursoDAO();
        Curso c = new Curso();

        c.setNome("Informatica"); 
        c.setProfessor(p);
        
        dao.save(c);

    }

}
