package br.com.senac.academico.dao;

import br.com.senac.academico.model.Aluno;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public abstract class DAO<T> {

    protected EntityManager em;

    private final Class<T> entidade;

    public DAO(Class<T> entidade) {
        this.entidade = entidade;
    }

    public void save(T entity) { //Salva
        
        this.em = JPAUtil.getEntityManager(); //Abrir conexao 
        em.getTransaction().begin(); //Comecei uma transacao
        em.persist(entity); //Inserir no banco
        em.getTransaction().commit(); //Fechar uma trasacao
        em.close(); //Fechar conexao

    }

    public void update(T entity) { //Atualiza
        
        this.em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.merge(entity); //Atualizar
        em.getTransaction().commit();
        em.close();

    }

    public void delete(T entity) {
        
        this.em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.remove(em.merge(entity)); 
        em.getTransaction().commit();
        em.close();
        
    }

    public T find(long id) {
        
        this.em = JPAUtil.getEntityManager(); 
        em.getTransaction().begin();
        T t = em.find(entidade, id);
        em.getTransaction().commit();
        em.close();
        
        return t;
    }

    public List<T> findAll() {
        
        this.em = JPAUtil.getEntityManager();
        List<T> lista;
        em.getTransaction().begin();
        lista = em.createQuery("from " + entidade.getName() + " e" ).getResultList() ;
        em.getTransaction().commit();
        em.close();
                
        return lista;
    }
    
    public List<Aluno> findByFiltro(String codigo , String nome){
        
        this.em = JPAUtil.getEntityManager();
        List<Aluno> lista;
        em.getTransaction().begin();
        Query query = em.createQuery("from Aluno a where" + "( :Id is not null and a.id = :Id" + "or" + " (:Nome is not null and a.nome like :Nome)");
        
        codigo = !codigo.isEmpty() ? codigo :null;
        nome = !nome.isEmpty() ? nome :null;
        
        query.setParameter("Id", codigo);
        query.setParameter("Nome", nome);
        
        lista = query.getResultList();
        
        em.getTransaction().commit();
        em.close();
        
        
        return lista;
       
        
    }
        
        
}
